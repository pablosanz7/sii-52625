//Pablo Sanz Hernández
// Raqueta.cpp: implementation of the Raqueta class.
//
//////////////////////////////////////////////////////////////////////

#include "Raqueta.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Raqueta::Raqueta()
{

}

Raqueta::~Raqueta()
{

}

void Raqueta::Dibuja()
{
	glColor3ub(0,255,255);
	glEnable(GL_LIGHTING);
	glPushMatrix();
	glTranslatef(posicion.x,posicion.y,0);
	glutSolidCube(3);
	glPopMatrix();

void Raqueta::Mueve(float t)
{
	posicion=posicion+velocidad*t;
}
